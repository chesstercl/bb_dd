﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormSalaDeVentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelProducto = New System.Windows.Forms.Label()
        Me.ComboBoxNombreProducto = New System.Windows.Forms.ComboBox()
        Me.TextBoxPrecioUnitario = New System.Windows.Forms.TextBox()
        Me.LabelPrecioUnitario = New System.Windows.Forms.Label()
        Me.NumericUpDownCantidadVendida = New System.Windows.Forms.NumericUpDown()
        Me.LabelCantidad = New System.Windows.Forms.Label()
        Me.TextBoxTotal = New System.Windows.Forms.TextBox()
        Me.LabelTotal = New System.Windows.Forms.Label()
        Me.ButtonAgregar = New System.Windows.Forms.Button()
        Me.DataGridViewVentas = New System.Windows.Forms.DataGridView()
        Me.PRODUCTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UNITARIO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CANTIDAD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SUBTOTAL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LabelTotalVenta = New System.Windows.Forms.Label()
        Me.TextBoxTotalVenta = New System.Windows.Forms.TextBox()
        CType(Me.NumericUpDownCantidadVendida, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridViewVentas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelProducto
        '
        Me.LabelProducto.AutoSize = True
        Me.LabelProducto.Location = New System.Drawing.Point(13, 13)
        Me.LabelProducto.Name = "LabelProducto"
        Me.LabelProducto.Size = New System.Drawing.Size(50, 13)
        Me.LabelProducto.TabIndex = 0
        Me.LabelProducto.Text = "Producto"
        '
        'ComboBoxNombreProducto
        '
        Me.ComboBoxNombreProducto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBoxNombreProducto.FormattingEnabled = True
        Me.ComboBoxNombreProducto.Location = New System.Drawing.Point(13, 30)
        Me.ComboBoxNombreProducto.Name = "ComboBoxNombreProducto"
        Me.ComboBoxNombreProducto.Size = New System.Drawing.Size(121, 21)
        Me.ComboBoxNombreProducto.TabIndex = 1
        '
        'TextBoxPrecioUnitario
        '
        Me.TextBoxPrecioUnitario.Location = New System.Drawing.Point(141, 30)
        Me.TextBoxPrecioUnitario.Name = "TextBoxPrecioUnitario"
        Me.TextBoxPrecioUnitario.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxPrecioUnitario.TabIndex = 2
        '
        'LabelPrecioUnitario
        '
        Me.LabelPrecioUnitario.AutoSize = True
        Me.LabelPrecioUnitario.Location = New System.Drawing.Point(141, 13)
        Me.LabelPrecioUnitario.Name = "LabelPrecioUnitario"
        Me.LabelPrecioUnitario.Size = New System.Drawing.Size(76, 13)
        Me.LabelPrecioUnitario.TabIndex = 3
        Me.LabelPrecioUnitario.Text = "Precio Unitario"
        '
        'NumericUpDownCantidadVendida
        '
        Me.NumericUpDownCantidadVendida.Location = New System.Drawing.Point(247, 31)
        Me.NumericUpDownCantidadVendida.Name = "NumericUpDownCantidadVendida"
        Me.NumericUpDownCantidadVendida.ReadOnly = True
        Me.NumericUpDownCantidadVendida.Size = New System.Drawing.Size(56, 20)
        Me.NumericUpDownCantidadVendida.TabIndex = 4
        '
        'LabelCantidad
        '
        Me.LabelCantidad.AutoSize = True
        Me.LabelCantidad.Location = New System.Drawing.Point(247, 12)
        Me.LabelCantidad.Name = "LabelCantidad"
        Me.LabelCantidad.Size = New System.Drawing.Size(49, 13)
        Me.LabelCantidad.TabIndex = 5
        Me.LabelCantidad.Text = "Cantidad"
        '
        'TextBoxTotal
        '
        Me.TextBoxTotal.Location = New System.Drawing.Point(310, 29)
        Me.TextBoxTotal.Name = "TextBoxTotal"
        Me.TextBoxTotal.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxTotal.TabIndex = 6
        '
        'LabelTotal
        '
        Me.LabelTotal.AutoSize = True
        Me.LabelTotal.Location = New System.Drawing.Point(310, 10)
        Me.LabelTotal.Name = "LabelTotal"
        Me.LabelTotal.Size = New System.Drawing.Size(31, 13)
        Me.LabelTotal.TabIndex = 7
        Me.LabelTotal.Text = "Total"
        '
        'ButtonAgregar
        '
        Me.ButtonAgregar.Location = New System.Drawing.Point(416, 26)
        Me.ButtonAgregar.Name = "ButtonAgregar"
        Me.ButtonAgregar.Size = New System.Drawing.Size(75, 23)
        Me.ButtonAgregar.TabIndex = 8
        Me.ButtonAgregar.Text = "Agregar"
        Me.ButtonAgregar.UseVisualStyleBackColor = True
        '
        'DataGridViewVentas
        '
        Me.DataGridViewVentas.AllowUserToDeleteRows = False
        Me.DataGridViewVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewVentas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PRODUCTO, Me.UNITARIO, Me.CANTIDAD, Me.SUBTOTAL})
        Me.DataGridViewVentas.Location = New System.Drawing.Point(13, 58)
        Me.DataGridViewVentas.Name = "DataGridViewVentas"
        Me.DataGridViewVentas.ReadOnly = True
        Me.DataGridViewVentas.Size = New System.Drawing.Size(500, 191)
        Me.DataGridViewVentas.TabIndex = 9
        '
        'PRODUCTO
        '
        Me.PRODUCTO.HeaderText = "PRODUCTO"
        Me.PRODUCTO.Name = "PRODUCTO"
        Me.PRODUCTO.ReadOnly = True
        Me.PRODUCTO.Width = 150
        '
        'UNITARIO
        '
        Me.UNITARIO.HeaderText = "UNITARIO"
        Me.UNITARIO.Name = "UNITARIO"
        Me.UNITARIO.ReadOnly = True
        '
        'CANTIDAD
        '
        Me.CANTIDAD.HeaderText = "CANTIDAD"
        Me.CANTIDAD.Name = "CANTIDAD"
        Me.CANTIDAD.ReadOnly = True
        '
        'SUBTOTAL
        '
        Me.SUBTOTAL.HeaderText = "SUB-TOTAL"
        Me.SUBTOTAL.Name = "SUBTOTAL"
        Me.SUBTOTAL.ReadOnly = True
        '
        'LabelTotalVenta
        '
        Me.LabelTotalVenta.AutoSize = True
        Me.LabelTotalVenta.Location = New System.Drawing.Point(141, 270)
        Me.LabelTotalVenta.Name = "LabelTotalVenta"
        Me.LabelTotalVenta.Size = New System.Drawing.Size(77, 13)
        Me.LabelTotalVenta.TabIndex = 10
        Me.LabelTotalVenta.Text = "Total de Venta"
        '
        'TextBoxTotalVenta
        '
        Me.TextBoxTotalVenta.Location = New System.Drawing.Point(224, 263)
        Me.TextBoxTotalVenta.Name = "TextBoxTotalVenta"
        Me.TextBoxTotalVenta.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxTotalVenta.TabIndex = 11
        Me.TextBoxTotalVenta.Text = "0"
        '
        'FormSalaDeVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(525, 299)
        Me.Controls.Add(Me.TextBoxTotalVenta)
        Me.Controls.Add(Me.LabelTotalVenta)
        Me.Controls.Add(Me.DataGridViewVentas)
        Me.Controls.Add(Me.ButtonAgregar)
        Me.Controls.Add(Me.LabelTotal)
        Me.Controls.Add(Me.TextBoxTotal)
        Me.Controls.Add(Me.LabelCantidad)
        Me.Controls.Add(Me.NumericUpDownCantidadVendida)
        Me.Controls.Add(Me.LabelPrecioUnitario)
        Me.Controls.Add(Me.TextBoxPrecioUnitario)
        Me.Controls.Add(Me.ComboBoxNombreProducto)
        Me.Controls.Add(Me.LabelProducto)
        Me.Name = "FormSalaDeVentas"
        Me.Text = "Sala de Ventas"
        CType(Me.NumericUpDownCantidadVendida, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridViewVentas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelProducto As Label
    Friend WithEvents ComboBoxNombreProducto As ComboBox
    Friend WithEvents TextBoxPrecioUnitario As TextBox
    Friend WithEvents LabelPrecioUnitario As Label
    Friend WithEvents NumericUpDownCantidadVendida As NumericUpDown
    Friend WithEvents LabelCantidad As Label
    Friend WithEvents TextBoxTotal As TextBox
    Friend WithEvents LabelTotal As Label
    Friend WithEvents ButtonAgregar As Button
    Friend WithEvents DataGridViewVentas As DataGridView
    Friend WithEvents PRODUCTO As DataGridViewTextBoxColumn
    Friend WithEvents UNITARIO As DataGridViewTextBoxColumn
    Friend WithEvents CANTIDAD As DataGridViewTextBoxColumn
    Friend WithEvents SUBTOTAL As DataGridViewTextBoxColumn
    Friend WithEvents LabelTotalVenta As Label
    Friend WithEvents TextBoxTotalVenta As TextBox
End Class
