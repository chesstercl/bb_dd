﻿Imports MySql.Data.MySqlClient

Public Class FormSalaDeVentas

    Dim precios As Double()
    Protected Sub FormSalaDeVentas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Conectar()
        Dim tabla = Seleccionar("SELECT nombre, unitario FROM productos")
        ReDim precios(tabla.Rows.Count)
        Dim i = 0
        For Each fila As DataRow In tabla.Rows
            ComboBoxNombreProducto.Items.Add(fila.Item("nombre").ToString())
            precios(i) = Integer.Parse(fila.Item("unitario").ToString())
            i += 1
        Next
        TextBoxPrecioUnitario.ReadOnly = True
        TextBoxTotal.ReadOnly = True
        NumericUpDownCantidadVendida.Enabled = False
        NumericUpDownCantidadVendida.ReadOnly = True
        TextBoxTotalVenta.ReadOnly = True

    End Sub

    Private Sub ComboBoxProducto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxNombreProducto.SelectedIndexChanged
        TextBoxPrecioUnitario.Text = precios(ComboBoxNombreProducto.SelectedIndex)
        NumericUpDownCantidadVendida.Enabled = True
    End Sub

    Private Sub NumericUpDownCantidad_ValueChanged(sender As Object, e As EventArgs) Handles NumericUpDownCantidadVendida.ValueChanged
        Dim precioUnitario = CDbl(TextBoxPrecioUnitario.Text)
        Dim cantidad = NumericUpDownCantidadVendida.Value
        If precioUnitario > 0 Then
            TextBoxTotal.Text = precioUnitario * cantidad
        End If
    End Sub

    Private Sub ButtonAgregar_Click(sender As Object, e As EventArgs) Handles ButtonAgregar.Click
        'Dim ventas
        Dim nombreProducto = ComboBoxNombreProducto.Text
        Dim precioUnitario = TextBoxPrecioUnitario.Text
        Dim cantidadVendida = NumericUpDownCantidadVendida.Value
        Dim total = TextBoxTotal.Text
        Dim totalVenta = TextBoxTotalVenta.Text

        If total = "" OrElse total = 0 Then
            MessageBox.Show("Tiene que completar todos los campos para agregar")
        Else
            DataGridViewVentas.Rows.Add({nombreProducto, precioUnitario, cantidadVendida, total})
            Insertar("INSERT INTO ventas(nombreProducto, precioUnitario, cantidadVendida, total)
                      VALUES ('" & nombreProducto & "'," & precioUnitario & "," & cantidadVendida & "," & total & ")")
            TextBoxTotalVenta.Text = CStr(CDbl(totalVenta) + CDbl(total))
        End If
    End Sub
End Class
