﻿Imports MySql.Data.MySqlClient

Module Datos

    Public conexion As MySqlConnection

    Sub Conectar()
        Try
            conexion = New MySqlConnection("server = localhost; 
                                            database = bd_negocio; 
                                            user id = root; 
                                            password = ;")
            conexion.Open()
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Sub Desconectar()
        Try
            conexion.Close()
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub


    Function Seleccionar(consulta As String) As DataTable
        Conectar()
        Dim dataAdapter = New MySqlDataAdapter(consulta, conexion)
        Dim dataTable As New DataTable
        dataTable.Clear()
        dataAdapter.Fill(dataTable)
        Return dataTable
        Desconectar()
    End Function

    Sub Insertar(consulta As String)
        Conectar()
        Dim insert As New MySqlCommand(consulta, conexion)
        Try
            insert.ExecuteNonQuery()
        Catch ex As MySqlException
            MessageBox.Show(ex.ToString)
        End Try
        Desconectar()
    End Sub
End Module
